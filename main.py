# Úloha pro uchazeče o pozici Python programátora

import matplotlib.pyplot as plt
import pandas as pd


def open_file(name_of_file):
    # opening log.run file
    file = open(name_of_file, "r")
    data = [z for z in file.readlines()]
    return data


def getting_data_k(data):
    # creating empty list for data collection : k
    k = []
    # getting need values, Final residual for k
    for x, i in enumerate(data):
        if i[:13] == 'ExecutionTime':
            value = data.pop(x - 1).split(' ')
            k.append(value[12][:-1])
    return k


def getting_data_omega(data):
    # creating empty list for data collection : omega
    omega = []
    # getting need values, Final residual for omega
    for x, i in enumerate(data):
        if i[:13] == 'ExecutionTime':
            value1 = data.pop(x - 2).split(' ')
            omega.append(value1[12][:-1])
    return omega


def getting_data_p_rgh(data):
    # creating empty list for data collection : p_rgh
    p_rgh = []
    # getting need values, Final residual for p_rgh
    for x, i in enumerate(data):
        if i[:13] == 'ExecutionTime':
            value2 = data.pop(x - 4).split(' ')
            p_rgh.append(value2[12][:-1])
    return p_rgh


def getting_data_time_value(data):
    # creating empty list for data collection : time_value
    time_value = []
    # getting all time data
    for i in data:
        if i[:6] == 'Time =':
            time_value.append(i[7:][:-1])
    return time_value


def creating_csv(time_value, p_rgh, omega, k):
    # Creating a table for saving all need data
    dict = {'time_value': time_value, 'p_rgh': p_rgh, 'omega': omega, 'k': k}
    df = pd.DataFrame(dict)
    # saving the dataframe
    df.to_csv('CheckResidual.csv')


def creating_graph(time_value, p_rgh, omega, k):
    # Plotting the points of graph
    plt.plot(time_value, omega, label="omega")
    plt.plot(time_value, k, label="k")
    plt.plot(time_value, p_rgh, label="p_rgh")

    # naming the x axis
    plt.xlabel('x -> Time')
    # naming the y axis
    plt.ylabel('y -> Final residual')

    # title to my graph
    plt.title('Graph!')

    # show a legend on the plot
    plt.legend()

    # function to show the plot
    plt.show()

# main function
def main():
    k = getting_data_k(open_file('log.run'))
    omega = getting_data_omega(open_file('log.run'))
    p_rgh = getting_data_p_rgh(open_file('log.run'))
    time_value = getting_data_time_value(open_file('log.run'))
    creating_csv(time_value, p_rgh, omega, k)
    creating_graph(time_value, p_rgh, omega, k)


if __name__ == '__main__':
    main()
