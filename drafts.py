# Úloha pro uchazeče o pozici Python programátora

import matplotlib.pyplot as plt
import pandas as pd

# opening log.run file
file = open("log.run", "r")
data=[z for z in file.readlines()]

# creating empty lists for data collection : p_rgh, k, omega
p_rgh=[]
k=[]
omega=[]

# getting need values
for x,i in enumerate(data):
    if i[:13]=='ExecutionTime':
        value = data.pop(x-1).split(' ')
        value1 = data.pop(x - 2).split(' ')
        value2 = data.pop(x-4).split(' ')
        # Final residual for k -> all data
        k.append(value[12][:-1])
        # Final residual for omega -> all data
        omega.append(value1[12][:-1])
        # Final residual for p_rgh -> all data
        p_rgh.append(value2[12][:-1])

# creating empty list for data collection : time_value
time_value =[]

for i in data:
    if i[:6] == 'Time =':
        # All time data
        time_value.append(i[7:][:-1])

# Creating a table for saving all need data
dict = {'time_value': time_value, 'p_rgh': p_rgh, 'omega': omega, 'k': k}
df = pd.DataFrame(dict)

# saving the dataframe
df.to_csv('CheckResidual.csv')

# Plotting the points of graph
plt.plot(time_value, omega, label = "omega")
plt.plot(time_value, k, label = "k")
plt.plot(time_value, p_rgh, label = "p_rgh")

# naming the x axis
plt.xlabel('x -> Time')
# naming the y axis
plt.ylabel('y -> Final residual')

# title to my graph
plt.title('Graph!')

# show a legend on the plot
plt.legend()

# function to show the plot
plt.show()
