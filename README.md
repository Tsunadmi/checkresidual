# CheckResidual_Technical_Task

   Připravte Python třídu CheckResidual, která:
- dovede načíst textový formát souboru log.run,
- najde časové kroky a rezidua veličin (viz žlutě výše – zajímá nás „Time“ tj. čas v sekundách
a „Final residual“ pro jednotlivé veličiny v rámci daného časového kroku – je-li uvedeno více
„Final residual“ pro téže veličinu, tak nás zajímá poslední z nich),
- vykreslí do grafu průběh reziduí (osa X je čas, osa Y je „Final residual“ v logaritmické škále,
jsou vynesené tři křivky „p_rgh“, „omega“ a „k“),
- uloží do souboru (ve formátu, který uznáte jako vhodný) čas a hodnoty reziduí pro další
zpracování – aby nebylo nutné v budoucnu opět „lovit“ hodnoty v dlouhém textovém logu


## Programming languages:

python 3.9:

## Virtual environment

Activate virtual environment in terminal in project folder:

```
venv/Scripts/activate
```

## Add important libraries

```
matplotlib~=3.6.1
pandas~=1.5.1

import pandas
import matplotlib.pyplot

```
All detailed  information you can find in **requirements.txt**

## Getting started

Start of app: **main.py**

```
python main.py
```

## Description

1. The first step is opening log.run with function --> ```def open_file(name_of_file)```

2. The second step is to find all data which we need:

-```getting_data_k(data)``` for Final residual of k.

-```getting_data_omega(data)``` for Final residual of omega.

-```getting_data_p_rgh(data)``` for Final residual of p_rgh.

-```getting_data_time_value(data)``` for all time data.

_In draft version i found all data in one cycle._

3. The third step is saving data to a CSV file with data which we have gotten--> ```creating_csv(time_value, p_rgh, omega, k)```.

Result is CSV file ```CheckResidual.csv``` _After I converted that to XLS file_ ```CheckResidual.xls```

4. The fourth step is growing graph with data which we have gotten--> ```creating_graph(time_value, p_rgh, omega, k)```.

Result is graph, in list of files you can see picture of this graph ```Figure_1.png```

## Author

This app was done by Dmitry Tsunaev.

- [ ] [LinkedIn](http://linkedin.com/in/dmitry-tsunaev-530006aa)
